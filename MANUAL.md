% md2term(1) | Manual de uso geral

## NOME

md2term - Parser de markdown para destaques e cores no terminal.

## DESCRIÇÃO

O `md2term` foi criado para ser um parser de markdown para textos curtos (menos de 30 linhas) que pudessem ser utilizados em apresentações pelo terminal (*slides* em texto) e, ao mesmo tempo, capazes de serem visualizados corretamente por outros parsers em sites na web (GitHub, GitLab, etc). O script também pode ser utilizado satisfatoriamente para visualizar outros textos em markdown, desde que observadas suas limitações.

## SINOPSE

md2term \[OPÇÕES\] -f ARQUIVO

## OPÇÕES


  -b CARACTERE\ \ \ Caractere marcador de listas.

  -c\ \ \ \ \ \ \ \ \ \ \ \ \ Limpa o terminal antes de exibir o texto.

  -i ESPAÇOS\ \ \ \ \ Indentação de listas, códigos e citações em espaços.

  -k VALOR\ \ \ \ \ \ \ Manter as cerquilhas nos títulos:

\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 0=Descarta todas 1=Mantém todas 2=Apenas no título 1

  -m ESPAÇOS\ \ \ \ \ Recuo da margem esquerda em espaços.

  -p\ \ \ \ \ \ \ \ \ \ \ \ \ Carrega em um pager (less).

  -s ARQUIVO\ \ \ \ \ Esquema de cores alternativo.

  -t LINHAS\ \ \ \ \ \ Afastamento do topo em linhas.

  -h\ \ \ \ \ \ \ \ \ \ \ \ \ Exibe esta ajuda.

## MARCAÇÕES VÁLIDAS

| Marcação | Resultado |
|---|---|
| `**TEXTO**` ou `__TEXTO__` | NEGRITO |
| `*TEXTO*` ou `*TEXTO*` | ITÁLICO |
| `___TEXTO___` ou `***TEXTO***` | NEGRITO E ITÁLICO |
| `~~TEXTO~~` | RISCADO |
| `<u>TEXTO</u>` | SUBLINHADO |
| ``` `TEXTO` ``` | CÓDIGO NA LINHA |
| `[RÓTULO](URL)` | RÓTULO |

## BLOCOS

As marcações dos blocos de código precisam ser feitas no ínicio da linha!

### Bloco de código

~~~
```
BLOCO DE CÓDIGO
```
~~~

### Listas não ordenadas

```
- Item 1
- Item 2
- Item 3
```

### Listas ordenadas

```
1. Item 1
1. Item 2
1. Item 3
```

### Citações/notas

```
> O texto da citação ou da nota.
```

### Forçar quebra de linha

```
Parágrafos, itens de lista e citações poder \
ter suas linhas quebradas onde desejado \
utilizando contrabarras.
```

## LIMITAÇÕES

- Não interpreta listas aninhadas (sub-itens) corretamente.
- As quebras de linhas longas podem acontecer no meio de palavras, mas é possível contornar o problema especificando onde elas devem acontecer no texto fonte (é uma marcação válida do markdown GFM).
- Utilizar as opções `-c` e `-p` em conjunto pode levar a problemas de redesenho do buffer no `less`.
- Links são apenas representações visuais utilizando o rótulo da marcação (não a URL). Se preferir a URL, ela pode ser usada como rótulo ou ser escrita como texto sem marcação (a maioria dos terminais interpreta URLs como link).

## LICENÇA

Copyright (C) 2022 Blau Araujo <blau@debxp.org>

Licença GPLv3+: GNU GPL versão 3 ou posterior <https://gnu.org/licenses/gpl.html> \
Este é um software livre: você é livre para alterá-lo e redistribuí-lo. \
NÃO HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei.

Desenvolvido por Blau Araujo e Romeu Alfa
